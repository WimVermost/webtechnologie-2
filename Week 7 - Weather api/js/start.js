/*global  $, Skycons*/
(function () {
	'use strict';
	//jSon
	var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	var d = new Date();
	var n = d.getDay();
	var App = {
		APIKEY: "a5dd3767bf47024363070c4af5590fd9",
		lat: "",
		lng: "",
		init: function () {
			App.getLocation();
		},
		getLocation: function () {
			// get the current user position	App.foundPos maakt nieuwe functie
			navigator.geolocation.getCurrentPosition(App.foundPosition);
		},
		foundPosition: function (pos) {
			// found the current user position
			App.lat = pos.coords.latitude;
			App.lng = pos.coords.longitude;
			App.getWeather();
		},
		getWeather: function () {
			//get current weather for my position.
			//units = si geeft alle data voor europese landen, celsius, km, ...
			var url = "https://api.forecast.io/forecast/" + App.APIKEY + "/" + App.lat + "," + App.lng + "?units=si";
			//JSONP
			window.jQuery.ajax({
				url: url,
				dataType: "jsonp",
				success: function (data) {

					// get and display city name
					var city = (data.timezone);
					city = city.substring(city.indexOf("/") + 1);
					$("#location").html(city);
					localStorage.setItem("city",city);
					
					// set time in localStorage
					var time = new Date().getTime();
                    localStorage.setItem("time", time);
					
					// set correct icon
					$('#weatherIcon').css('background-image', 'url(' + getUrl(data.currently.icon) + ')');
					localStorage.setItem("icon", getUrl(data.currently.icon));
					
					// set current weather
					$(".todayWeather").html(Math.round(data.currently.temperature) + "°C");
					localStorage.setItem("currentTemp",Math.round(data.currently.temperature));

					// set sunset-sunrise
					
					$(".sunrise").html(getTime(data.daily.data[0].sunriseTime));
					$(".sunset").html(getTime(data.daily.data[0].sunsetTime));
					localStorage.setItem("sunrise",getTime(data.daily.data[0].sunriseTime));
					localStorage.setItem("sunset",getTime(data.daily.data[0].sunsetTime));

					// setting max temp
					for (var i = 0; i < 5; i++) {
						// setting max temp
						$(".weather" + i + "").html(Math.round(data.daily.data[i].temperatureMax) + "°C");
						localStorage.setItem("dayMax"+i, Math.round(data.daily.data[i].temperatureMax));
						
						// setting min temp
						$("#weather" + i + "min").html(Math.round(data.daily.data[i].temperatureMin) + "°C");
						localStorage.setItem("dayMin"+i, Math.round(data.daily.data[i].temperatureMin));
					}

					// set day

					for (var i = 0; i <  5; i++) {
						console.log(i);
						if (n >= 6) {
							n = 0;
						}
						$("#day" + i + "").html(days[n]);
						n = n + 1;
					}

				}
			});
		}
	};
	
	function getUrl(icon) {
		var imgUrl;
		switch (icon) {
		case "clear-day":
			imgUrl = "icons/Sun.svg";
			break;
		case "clear-night":
			imgUrl = "icons/Cloud-Moon.svg";
			break;
		case "partly-cloudy-night":
			imgUrl = "icons/Cloud-Moon.svg";
			break;
		case "partly-cloudy-day":
			imgUrl = "icons/Cloud-Sun.svg";
			break;
		case "rain":
			imgUrl = "icons/Cloud-Rain.svg";
			break;
		case "cloudy":
			imgUrl = "icons/Cloud.svg";
			break;
		case "snow":
			imgUrl = "icons/Cloud-Snow.svg";
			break;
		case "wind":
			imgUrl = "icons/Wind.svg";
			break;
		default:
			imgUrl = "icons/Cloud.svg";

		}
		return imgUrl;
	}
	
	function getTime(time){
		
		var date = new Date(time * 1000);
		var hours = date.getHours();
		var minutes = "0" + date.getMinutes();
		var correctTime = hours + ':' + minutes.substr(-2);
		return correctTime;

	}
	
	var now = new Date().getTime();

    if(localStorage.getItem("time") === null){
		console.log("Data coming from Forecast.io...");
        App.init();
		
    }else if((parseInt(localStorage.getItem("time"))+ 3600000 > now)){
		console.log("Data coming from localstorage...");
		// set everything from localstorage...
		$("#location").html(localStorage.getItem("city"));
		$('#weatherIcon').css('background-image', 'url(' + localStorage.getItem("icon") + ')');
		$(".todayWeather").html(localStorage.getItem("currentTemp") + "°C");
		$(".sunrise").html(localStorage.getItem("sunrise"));
		$(".sunset").html(localStorage.getItem("sunset"));
		
		for (var i = 0; i < 5; i++) {
			// setting max temp
			$(".weather" + i + "").html(localStorage.getItem("dayMax"+ i) + "°C");
			// setting min temp
			$("#weather" + i + "min").html(localStorage.getItem("dayMin"+ i) + "°C");
			
		}	
		for (var i = 0; i < 5; i++) {
			console.log(i);
			if (n >= 6) {
				n = 0;
			}
			$("#day" + i + "").html(days[n]);
			n = n + 1;
		}
		
	}
	else{
		console.log("Data coming from Forecast.io...");
		App.init();
	}
	
}());
 
 	