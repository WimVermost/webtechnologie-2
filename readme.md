# Wim Vermost Portfolio


[Link naar het eindwerk Webtech 2 (Q&A)](https://bitbucket.org/WimVermost/forum)

---

![Icon Wim Vermost](http://vermost.net/images/logoblack.png)


## Overview
* GIT
* WeAreIMD Flexbox
* Marvel Flexbox
* Todo app with advanced JS
* Basic Node routing
* Use of a weather api
* BEM
* GULP
* Workshop district 01


### GIT


---

### WeAreIMD Flexbox


---

### Marvel Flexbox


---

### TODO app with advanced JS


---

### Basic Node routing


---

### Weather app made with the Forecast.io API


---

### BEM


---

### GULP.js


---

### Workshop District 01


---



